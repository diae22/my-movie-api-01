from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.user import user_router
from routers.computadora import computadora_router

app = FastAPI()
app.title = "Cosmobius X API"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)
app.include_router(computadora_router)

Base.metadata.create_all(bind = engine)

# movies = [
#     {
#         "id": 1,
#         "title": "Avatar",
#         "overview": "En un exuberante planeta llamado Pandora viven unos monos azules",
#         "year": "2009",
#         "rating": 7.8,
#         "category": "Accion"
#     },
#     {
#         "id": 2,
#         "title": "The Hunger Games on Fire",
#         "overview": "Best Movie Ever",
#         "year": "2014",
#         "rating": 10.0,
#         "category": "Accion"
#     },
#     {
#         "id": 3,
#         "title": "Madame Web",
#         "overview": "La mejor pelicula de madame web de la historia. Definitivamente una de las peliculas de la historia",
#         "year": "2024",
#         "rating": 10.0,
#         "category": "Accion"
#     }
# ]

@app.get('/', tags = ['home'])
def message():
    return HTMLResponse('<h1> Hello World </h1>')


# computadoras = [
#     {
#         "id": 1,
#         "marca": "Asus",
#         "modelo": "TUF",
#         "color": "negro",
#         "ram": 16,
#         "almacenamiento": "1TB"
#     },
#     {
#         "id": 2,
#         "marca": "Asus",
#         "modelo": "ROG",
#         "color": "negro",
#         "ram": 34,
#         "almacenamiento": "2TB"
#     },
#     {
#         "id": 3,
#         "marca": "Lenovo",
#         "modelo": "IdeaPad",
#         "color": "Plateado",
#         "ram": 8,
#         "almacenamiento": "500GB"
#     },
#     {
#         "id": 4,
#         "marca": "Lenovo",
#         "modelo": "ThinkPad",
#         "color": "Plateado",
#         "ram": 16,
#         "almacenamiento": "1TB"
#     },
#     {
#         "id": 5,
#         "marca": "Apple",
#         "modelo": "MacBook Pro",
#         "color": "Blanco",
#         "ram": 16,
#         "almacenamiento": "500GB"
#     }
# ]

# venv\Scripts\activate

# uvicorn main:app

# uvicorn main:app --reload  para que se actualizen los cambios automaticamente

# uvicorn main:app --reload --port  3000  para usar un puerto en especifico

# uvicorn main:app --reload --port  3000 --host 0.0.0.0  para que sea publico

#172.18.70.235:3000/docs

# Realiza los end points para la venta de computadoras con una 
# lista de 5 registros con los siguientes valores: id, marca, modelo, color, ram y 
# almacenamiento. Realiza los end points ya hechos en clase. En lugar de ser categorias 
# sea get by marca para obtener todas las compus con las mismas marcas de manera que se 
# obtenga todo lo de la computadora, en caso de ser mas de una, mostrara todas las que sean 
# de la misma marca

# @app.put('/computadoras/{id}', tags=['computadoras'])
# def update_compu(id: int, marca: str = Body(), modelo: str = Body(), color: str = Body(), ram: int = Body(), almacenamiento: str = Body()):
#     for item in computadoras:
#         if item["id"] == id:
#             item["marca"] == marca,
#             item["modelo"] == modelo,
#             item["color"] == color,
#             item["ram"] == ram,
#             item["almacenamiento"] == almacenamiento
    
#     return computadoras


# @app.post('/movies', tags=['movies'])
# def create_movie(id: int = Body(), title: str = Body(), overview: str = Body(), year: int = Body(), rating: float = Body(), category: str = Body()):
#     movies.append({
#         "id": id,
#         "title": title,
#         "overview": overview,
#         "year": year,
#         "rating": rating,
#         "category": category
#     })
#     return movies


# @app.put('/movies/{id}', tags=['movies'])
# def update_movie(id: int, title: str = Body(), overview: str = Body(), year: int = Body(), rating: float = Body(), category: str = Body()):
#     for item in movies:
#         if item["id"] == id:
#             item["title"] == title,
#             item["overview"] == overview,
#             item["year"] == year,
#             item["rating"] == rating,
#             item["category"] == category
    
#     return movies


# Existen dos tipos de parametros: ruta y query
# Codigo de estado: son como el 404 not found. Existe el 200 que solo es un ok

