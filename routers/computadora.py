from fastapi import Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from models.computadora import Computadora as ComputadoraModel
from config.database import Session
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from fastapi import APIRouter

computadora_router = APIRouter()

class Computadora(BaseModel):
    id: Optional[int] = None
    marca: str = Field(min_length = 1, max_length = 15)
    modelo: str = Field(min_length = 1, max_length = 50)
    color: str = Field(min_length = 1)
    ram: int = Field(ge = 1)
    almacenamiento: str = Field(min_length = 2)

    class Config:
        json_schema_extra = {
            "example": {
                "id": 1,
                "marca": "Asus",
                "modelo": "TUF",
                "color": "negro",
                "ram": 16,
                "almacenamiento": "1TB"
            }
        }

@computadora_router.get('/computadoras', tags=["computadoras"], response_model = List[Computadora])
def get_compus() -> list[Computadora]:
    db = Session()
    result = db.query(ComputadoraModel).all()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@computadora_router.get('/computadoras/{id}', tags=["computadoras"], response_model = Computadora )
def get_by_id(id: int) -> Computadora:
    db = Session()
    result = db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()
    if not result:
        return JSONResponse(status_code = 404, content={'message': "No encontrado"})
    return JSONResponse(status_code=200,content=jsonable_encoder(result))

@computadora_router.get('/computadoras/', tags=["computadoras"], response_model =  list[Computadora])
def get_by_marca(marca: str):
    db = Session()
    result = db.query(ComputadoraModel).filter(ComputadoraModel.marca == marca).all()
    return JSONResponse(status_code=200,content=jsonable_encoder(result))

@computadora_router.post('/computadoras', tags=['computadoras'], response_model = dict)
def create_compu(computadora: Computadora) -> dict:
    db = Session()
    new_computadora = ComputadoraModel(**computadora.model_dump())
    db.add(new_computadora)
    db.commit()
    return JSONResponse(status_code=200,content={"message": "Se ha registrado la computadora"})

@computadora_router.delete('/computadoras/{id}', tags=['computadoras'], response_model = dict)
def delete_compu(id: int) -> dict:
    db = Session()
    result = db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()
    if not result:
        return JSONResponse(status_code = 404, content = {'message': "No encontrado"})
    db.delete(result)
    db.commit()
    return JSONResponse(status_code=200,content={"message": "Se ha eliminado la computadora"})

@computadora_router.put('/computadoras/{id}', tags=['computadoras'], response_model = dict)
def update_compu(id: int, computadora: Computadora) -> dict:
    db = Session()
    result = db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()
    if not result:
        return JSONResponse(status_code = 404, content = {'message': "No encontrado"})
    result.marca = computadora.marca
    result.modelo = computadora.modelo
    result.color = computadora.color
    result.ram = computadora.ram
    result.almacenamiento = computadora.almacenamiento
    db.commit()
    return JSONResponse(status_code = 200, content={'message': "Se ha modificado la computadora"})