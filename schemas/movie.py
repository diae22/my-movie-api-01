from pydantic import BaseModel, Field
from typing import Optional

class Movie(BaseModel):
    # id: int | None = None
    id: Optional[int] = None
    title: str = Field(min_length = 1, max_length = 25)
    overview: str = Field(min_length = 1, max_length = 100)
    year: int = Field(le = 2024)
    rating: float = Field(ge = 1, le = 10)
    category: str = Field(min_length = 2)

    class Config:
        json_schema_extra = {
            "example": {
                "id": 1,
                "title": "Mi pelicula",
                "overview": "Descripcion de pelicula",
                "year": 2024,
                "rating": 6.6,
                "category": "Accion"
            }
        }